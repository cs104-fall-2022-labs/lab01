number_of_friends = 3
tasdelen_rent = 1300
cekmekoy_rent = 1500
uskudar_rent = 1800
kadikoy_rent = 2000
monthly_contribution = 10

rent_for_each_person_in_tasdelen = (tasdelen_rent * (100 + monthly_contribution) / 100) / number_of_friends
rent_for_each_person_in_cekmekoy = (cekmekoy_rent * (100 + monthly_contribution) / 100) / number_of_friends
rent_for_each_person_in_uskudar = (uskudar_rent * (100 + monthly_contribution) / 100) / number_of_friends
rent_for_each_person_in_kadikoy = (kadikoy_rent * (100 + monthly_contribution) / 100) / number_of_friends

print("Tasdelen: ", rent_for_each_person_in_tasdelen)
print("Cekmekoy: ", rent_for_each_person_in_cekmekoy)
print("Uskudar: ", rent_for_each_person_in_uskudar)
print("Kadikoy: ", rent_for_each_person_in_kadikoy)
